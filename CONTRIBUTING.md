# Contributing

TODO: Augment this contributor guide

## Style Checking

Our CI pipeline uses two tools to do style checks, Ruff and Black. Their options are defined on the `pyproject.toml` file, so if you run these tools locally from the root of the repo, you will be using the same rules as the pipeline. To install the tools on your local machine, simply do:

```bash
pip3 install --user ruff black
```

And then run:

```bash
ruff check --diff .
black --check --diff --color .
```

To see the errors on your changes. You can also ask for the tools to try to fix some of the errors automatically by doing:

```bash
ruff check --fix .
black .
```
