"""_summary_"""

import os

from sqlalchemy import MetaData
from sqlalchemy_schemadisplay import create_schema_graph

DEFAULT_DATABASE_URL = "://insights4ci:insights4ci@127.0.0.1:3306/insights4ci"

DB = os.environ.get("DATABASE_URL", DEFAULT_DATABASE_URL)
graph = create_schema_graph(
    metadata=MetaData("mariadb" + DB),
    show_datatypes=False,
    show_indexes=False,
    rankdir="LR",
    concentrate=False,
)

graph.write_png("dbschema.png")
