"""Unitary tests for api-server. Should run as part of a CI pipeline"""

from fastapi.testclient import TestClient
from insights4ci.database import Base, get_db
from insights4ci.main import app
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

SQLALCHEMY_DATABASE_URL = (
    "mariadb+mariadbconnector://test:test@127.0.0.1:3307/insights4ci_test"
)
OK = 200
BAD_REQUEST = 400
NOT_FOUND = 404

engine = create_engine(SQLALCHEMY_DATABASE_URL)

TestingSessionLocal = sessionmaker(
    autocommit=False, autoflush=False, bind=engine
)


for tbl in reversed(Base.metadata.sorted_tables):
    engine.execute(tbl.delete())

Base.metadata.create_all(bind=engine)


def override_get_db():
    """_summary_

    Yields
    ------
    _type_
        _description_
    """
    local_db = TestingSessionLocal()
    try:
        yield local_db
    finally:
        local_db.close()


app.dependency_overrides[get_db] = override_get_db


client = TestClient(app)


def test_read_projects():
    """_summary_"""
    response = client.get("/projects")
    assert response.status_code == OK


def test_read_project_not_found():
    """_summary_"""
    response = client.get("/projects/42")
    assert response.status_code == NOT_FOUND


def test_create_project():
    """_summary_"""

    def check_view_fields():
        fields = ("id", "name", "description", "latest_pipeline")
        assert set(fields).issubset(data.keys())
        assert data["name"] == name
        assert data["description"] == description

    name = "just-a-project"
    description = "just a description"
    response = client.post(
        "/projects/", json={"name": name, "description": description}
    )
    assert response.status_code == OK
    data = response.json()

    check_view_fields()

    project_id = data.get("id")
    response = client.get(f"/projects/{project_id}")
    assert response.status_code == OK, response.text
    data = response.json()

    check_view_fields()


def test_create_project_with_same_name():
    """_summary_"""
    name = "just-a-project"
    description = "just a description"
    response = client.post(
        "/projects/", json={"name": name, "description": description}
    )
    assert response.status_code == BAD_REQUEST
    data = response.json()
    assert data["detail"] == "Project already registered."


def test_read_runners():
    """_summary_"""
    response = client.get("/runners")
    assert response.status_code == OK


def test_read_runner_not_found():
    """_summary_"""
    response = client.get("/runners/42")
    assert response.status_code == NOT_FOUND


def test_create_runner():
    """_summary_"""

    def check_view_fields():
        fields = (
            "id",
            "name",
            "description",
            "external_id",
            "last_seen",
            "architecture",
            "platform",
            "owner",
        )
        assert set(fields).issubset(data.keys())
        assert data["name"] == name
        assert data["description"] == description

    name = "just-a-runner"
    description = "just a description"
    response = client.post(
        "/runners/", json={"name": name, "description": description}
    )
    assert response.status_code == OK
    data = response.json()

    check_view_fields()

    runner_id = data.get("id")
    response = client.get(f"/runners/{runner_id}")
    assert response.status_code == OK, response.text
    data = response.json()

    check_view_fields()


def test_create_runner_with_same_name():
    """_summary_"""
    name = "just-a-runner"
    description = "just a description"
    response = client.post(
        "/runners/", json={"name": name, "description": description}
    )
    assert response.status_code == BAD_REQUEST
    data = response.json()
    assert data["detail"] == "Runner already registered."
