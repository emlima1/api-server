"""
Note: SQLAlchemy uses the term "model" to refer to these classes and instances
that interact with the database.

But Pydantic also uses the term "model" to refer to something different, the
data validation, conversion, and documentation classes and instances.

To avoid confusion between the SQLAlchemy models and the Pydantic models, we
will have the file models.py with the SQLAlchemy models, and the file
schemas.py with the Pydantic models.

These Pydantic models define more or less a "schema" (a valid data shape).
"""

from datetime import datetime
from typing import Optional

from pydantic import BaseModel, HttpUrl


class User(BaseModel):
    id: int
    email: str
    enabled: bool

    class Config:
        orm_mode = True


class RunnerCreate(BaseModel):
    name: str
    owner: Optional[str] = None
    external_id: Optional[str] = None
    description: Optional[str] = None
    architecture: Optional[str] = None
    platform: Optional[str] = None


class Runner(RunnerCreate):
    id: int

    class Config:
        orm_mode = True


class TestResultAloneCreate(BaseModel):
    status: str
    execution_time: Optional[float] = None


class TestResultCreate(TestResultAloneCreate):
    name: str
    class_name: str
    weight: Optional[float] = None


class JobCommon(BaseModel):
    external_id: str
    name: str
    status: str
    stage: Optional[str] = None
    external_url: Optional[str] = None
    ref: Optional[str] = None
    allow_failure: Optional[bool] = False
    commit: Optional[str] = None
    created_at: Optional[datetime] = None
    started_at: Optional[datetime] = None
    finished_at: Optional[datetime] = None


class JobCreate(JobCommon):
    runner: Optional[RunnerCreate] = None
    test_results: list[TestResultCreate]


class Job(JobCommon):
    id: int
    tests_length: Optional[int] = 0
    runner: Optional[Runner] = None

    class Config:
        orm_mode = True


class PipelineCreate(BaseModel):
    external_id: str
    status: str
    sha: Optional[str] = None
    ref: Optional[str] = None
    source: Optional[str] = None
    created_at: datetime
    updated_at: Optional[datetime] = None
    external_url: Optional[str] = None


class Pipeline(PipelineCreate):
    id: int
    tests_stats: Optional[dict] = {}
    jobs: list[Job]

    class Config:
        orm_mode = True


class LatestPipeline(BaseModel):
    id: int
    status: str
    sha: Optional[str] = None
    ref: Optional[str] = None
    source: Optional[str] = None
    created_at: datetime
    updated_at: Optional[datetime] = None
    external_url: Optional[str] = None

    class Config:
        orm_mode = True


class ProjectCreate(BaseModel):
    name: str
    description: Optional[str] = None
    repository_url: Optional[HttpUrl] = None


class Project(ProjectCreate):
    id: int
    tests_length: Optional[int] = 0
    runners_length: Optional[int] = 0
    latest_pipeline: Optional[LatestPipeline] = None

    class Config:
        orm_mode = True


class TestResult(TestResultAloneCreate):
    job: Optional[Job] = None

    class Config:
        orm_mode = True


class TestDetail(BaseModel):
    id: int
    name: str
    class_name: str
    weight: Optional[float]
    test_results: list[TestResult]
    results_commit_history: Optional[dict]

    class Config:
        orm_mode = True
