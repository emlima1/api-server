"""Summary"""

import os

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

# 127.0.0.1 must be used so mysql can connect using TCP. Please do not use
# 'localhost' otherwise it will try to connect to a local unix socket.
DEFAULT_DB_URL = "://insights4ci:insights4ci@127.0.0.1:3306/insights4ci"
DB_URL = os.environ.get("DATABASE_URL", DEFAULT_DB_URL)
SQLALCHEMY_DATABASE_URL = f"mariadb+mariadbconnector{DB_URL}"

engine = create_engine(
    # SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
    SQLALCHEMY_DATABASE_URL
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


# Dependency magic with yield
# https://fastapi.tiangolo.com/tutorial/dependencies/dependencies-with-yield/
def get_db():
    """_summary_

    Yields
    ------
    _type_
        _description_
    """
    local_db = SessionLocal()
    try:
        yield local_db
    finally:
        local_db.close()
